import sqlite3

conn, cursor = None, None


def connect_db(db_path):
    global conn, cursor
    try:
        conn = sqlite3.connect(db_path, check_same_thread=False)
        cursor = conn.cursor()
        return True
    except sqlite3.Error as e:
        print(f"Error connecting to the database: {e}")
        return False


def close_db():
    global conn
    if conn:
        conn.close()


def query_db(table_name, sender, receiver, subject, message_body, datetime):
    global cursor
    try:
        cursor.execute(f"SELECT * FROM {table_name} WHERE Sender='{sender}' AND Subject='{subject}'")
        existing_data = cursor.fetchone()

        if existing_data:
            cursor.execute(
                f"INSERT INTO {table_name} (Subject, Sender, Body, Receiver, DateTime) VALUES (?, ?, ?, ?, ?)",
                (subject, sender, message_body, receiver, datetime))
            conn.commit()
            print('Data successfully inserted')
        else:
            print('No matching found in database')

    except sqlite3.Error as e:
        print(f"Error executing SQL query: {e}")
