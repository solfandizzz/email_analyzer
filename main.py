import email
from email.header import decode_header
import imaplib
import keyboard
import re
import time
import json
import database
import sys

global conf


def initialize(connect_required=True):
    global conf
    with open('config.json') as f:
        conf = json.load(f)

    if connect_required:
        if not database.connect_db(conf['Params']['database_path']):
            print("Error connecting to the database. Exiting...")
            sys.exit()


    # Source emails
    email_addresses = conf['SenderEmails']['email_list']

    # Login info
    email_address = conf['Login']['email']
    password = conf['Login']['password']

    mail = imaplib.IMAP4_SSL(conf['MailServer']['gmail'])
    mail.login(email_address, password)

    mail.select(conf['Params']['folders'][0])

    return email_addresses, mail


def process_new_mail(email_addresses, mail):
    global conf
    for addr in email_addresses:
        status, messages = mail.search(None, f'(UNSEEN FROM "{addr}")')
        message_ids = messages[0].split()
        for message_id in message_ids:
            _, msg_data = mail.fetch(message_id, "(RFC822)")
            raw_email = msg_data[0][1]
            msg = email.message_from_bytes(raw_email)

            if msg["Subject"] is None:
                print('Error: msg["Subject"] is None')
                return

            subject, encoding = decode_header(msg["Subject"])[0]
            if isinstance(subject, bytes):
                subject = subject.decode(encoding if encoding else "utf-8")

            sender = msg.get('From')
            match = re.search(r'[\w.+-]+@[\w-]+\.[\w.-]+', sender)
            sender_email = match.group() if match else None

            receiver = msg.get('To')

            datetime = msg.get('Date')

            content = ''
            for part in msg.walk():
                if part.get_content_type() == conf['Params']['content_types'][0]:
                    content = part.get_payload(decode=True).decode(part.get_content_charset() or 'utf-8')

            print(f"New Mail:")
            print(f"Subject: {subject}")
            print(f"From: {sender}")
            print(f"To: {receiver}")
            print(f"Date: {datetime}")
            print(f"Content: {content}")
            print("-----")

            database.query_db(conf['Params']['table_name'], sender_email, receiver, subject, content, datetime)


def main_loop(addr, mail):
    while True:
        if keyboard.is_pressed('Esc'):
            database.close_db()
            mail.close()
            break
        process_new_mail(addr, mail)
        time.sleep(1)


if __name__ == '__main__':
    addr_list, mail_obj = initialize()
    if addr_list is not None and mail_obj is not None:
        print('Status OK')
        main_loop(addr_list, mail_obj)
    else:
        print("Error initializing. Mail is None")


