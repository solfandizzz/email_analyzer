# Email_analyzer



## Описание конфигурации
```

"Login": {
    "email": "test@mail.ru",
    "password": "someExternalKey"
}
```
Логин и пароль почты на который будут приходить все письма. Ключ должен быть внешний.

```
"MailServer": {
    "mail": "imap.mail.ru",
    "gmail": "imap.gmail.com",
    "yandex": "imap.yandex.kz"
}
```
IMAP-сервера для почтовых сервисов.
```
"SenderEmails": {
    "email_list": ["example@gmail.com","another@gmail.com"]
}
```
Список адресов электронной почты, которые будут использоваться в качестве отправителей
```
"Params": {
    "folders": ["Inbox","Sent","Spam","Trash"],
    "database_path": "email.db",
    "query_modes": ["UNSEEN", "ALL"],
    "content_types": ["text/plain", "text/html", "multipart/alternative", "multipart/mixed"],
    "table_name": "Mails"
}
```
**folders**: список почтовых вкладок/папок, из которых будут читаться письма.
**database_path**: путь к базе данных(в данном случае SQLite)

Параметры для будущей модификации:

**query_modes**: все письма, либо новые/непрочитанные 
 
**content_types**: ["text/plain", "text/html", "multipart/alternative", "multipart/mixed"] - типы контента, которые будут учитываться при обработке писем.

**table_name**:  имя таблицы в базе данных, где будут храниться данные о письмах.
